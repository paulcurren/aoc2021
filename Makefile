# Tested/linted package paths
TEST_DIRS := ./pkg/... ./days/...

all: build
.PHONY: all

# Build/test/docs output dirs.
target:
	mkdir -p target/

clean: target
	rm -rf target/*
.PHONY: clean

ALL_TARGETS := $(patsubst days/%/main.go, target/%, $(wildcard days/*/main.go))

$(info hello $(ALL_TARGETS))
#echo 'test message'
# @echo All targets are ${ALL_TARGETS}

$(ALL_TARGETS): target/%: days/%
	$(eval BINARY := $(patsubst target/%, %, $@))
	GOOS=$(GOOS) go build -a -installsuffix cgo -ldflags="-s -w" -o target/$(BINARY)/$(BINARY) ./$<

build: target
.PHONY: build