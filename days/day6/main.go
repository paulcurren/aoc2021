package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type lanternFishSchool struct {
	spawnCounter []int // each index holds a count of the number of fish at that 'spawn time'
}

func NewLanternFishSchool() *lanternFishSchool {
	return &lanternFishSchool{
		spawnCounter: make([]int, 9),
	}
}

// tick marks the passing of a single day and updates the school based, taking into account
// reproduction
func (l *lanternFishSchool) tick() {
	// all the '0' fish create a new '8' fish
	//     all the '0' fish become '6' fish
	// every other fish moves to the next lower number
	// Note: Don't count down the newly created day '8' fish. But do count do the existing one
	numNewFish := 0
	for fishDay := 0; fishDay < len(l.spawnCounter); fishDay++ {
		switch fishDay {
		case 0:
			numNewFish = l.spawnCounter[fishDay]
		default:
			l.spawnCounter[fishDay-1] = l.spawnCounter[fishDay] // move every fish down a category - 8 is untouched
		}
	}

	l.spawnCounter[8] = numNewFish
	l.spawnCounter[6] += numNewFish
}

// population counts how many fish there are altogether
func (l *lanternFishSchool) population() int {
	total := 0
	for _, num := range l.spawnCounter {
		total += num
	}

	return total
}

// parsePopulation expects a comma separated list of integers between 0 and 8
func (l *lanternFishSchool) parsePopulation(population string) error {
	scanner := bufio.NewScanner(strings.NewReader(population))
	scanner.Split(func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if atEOF && len(data) == 0 {
			return 0, nil, nil
		}
		if i := bytes.IndexByte(data, ','); i >= 0 {
			// We have a full comma terminated token
			return i + 1, data[0:i], nil
		}
		// If we're at EOF, we have a final, non-terminated token. Return it.
		if atEOF {
			return len(data), data, nil
		}
		// Request more data.
		return 0, nil, nil
	})

	for scanner.Scan() {
		fishDay := scanner.Text()

		var i int
		var err error
		if i, err = strconv.Atoi(fishDay); err != nil {
			return fmt.Errorf("error reading fish item %v. err = %v", fishDay, err)
		}

		if i < 0 || i > 8 {
			return fmt.Errorf("fish item out of range = %d", i)
		}

		l.spawnCounter[i]++
	}

	return nil
}

func main() {
	part1and2()
}

func part1and2() {
	filename := "./data/input.txt"

	file, err := os.Open(filename)

	if err != nil {
		fmt.Printf("error opening file %v. err = %v\n", filename, err)
		return
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	if !scanner.Scan() {
		fmt.Printf("no lines in the input file %v\n", filename)
	}

	line := scanner.Text()
	school := NewLanternFishSchool()
	if err = school.parsePopulation(line); err != nil {
		fmt.Printf("error parsing the population. err = %v", err)
	}

	for i := 0; i < 256; i++ {
		school.tick()
	}

	fmt.Printf("Population = %d\n", school.population())
}
