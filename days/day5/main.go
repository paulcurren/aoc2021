package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
)

type point struct {
	x int
	y int
}

func newPoint(x int, y int) *point {
	return &point{x, y}
}

type ventLine struct {
	start point
	end   point
}

func newVentLine(start point, end point) *ventLine {
	return &ventLine{start, end}
}

// pointsCovered an array of all the points this line covers
func (v *ventLine) pointsCovered() []point {
	line := make([]point, 0)

	if v.isHorizontal() {
		if v.swapDirection() {
			for i := v.end.x; i <= v.start.x; i++ {
				line = append(line, *newPoint(i, v.end.y))
			}
		} else {
			for i := v.start.x; i <= v.end.x; i++ {
				line = append(line, *newPoint(i, v.end.y))
			}
		}
	} else if v.isVertical() {
		// iterate over the y points - x stays the same
		if v.swapDirection() {
			for i := v.end.y; i <= v.start.y; i++ {
				line = append(line, *newPoint(v.end.x, i))
			}
		} else {
			for i := v.start.y; i <= v.end.y; i++ {
				line = append(line, *newPoint(v.end.x, i))
			}
		}
	} else if v.isDiagonal() { //part2
		xUp := v.start.x < v.end.x
		yUp := v.start.y < v.end.y
		diff := int(math.Abs(float64(v.start.x - v.end.x)))

		x := v.start.x
		y := v.start.y
		line = append(line, *newPoint(x, y))
		for i := 0; i < diff; i++ {
			if xUp {
				x++
			} else {
				x--
			}

			if yUp {
				y++
			} else {
				y--
			}
			line = append(line, *newPoint(x, y))
		}
	}

	return line
}

func (v *ventLine) isHorizontal() bool {
	return v.start.y == v.end.y
}

func (v *ventLine) isVertical() bool {
	return v.start.x == v.end.x
}

func (v *ventLine) isDiagonal() bool {
	return math.Abs(float64(v.start.x-v.end.x)) == math.Abs(float64(v.start.y-v.end.y))
}

// swapDirection returns true if the line should be plotted from end to start (end co-ord is lower than start)
func (v *ventLine) swapDirection() bool {
	if v.isHorizontal() {
		// compare x points
		return v.start.x > v.end.x
	} else {
		return v.start.y > v.end.y
	}
}

type overlaps struct {
	pointCounter map[point]int
}

func newOverlaps() *overlaps {
	return &overlaps{
		make(map[point]int),
	}
}

func (o *overlaps) plotVentLine(v ventLine) {
	line := v.pointsCovered()
	for _, p := range line {
		o.plotPoint(p)
	}
}

func (o *overlaps) plotPoint(p point) {
	v, ok := o.pointCounter[p]
	if ok {
		v++
		o.pointCounter[p] = v
	} else {
		o.pointCounter[p] = 1
	}
}

func (o *overlaps) countCrossovers() int {
	count := 0
	for _, v := range o.pointCounter {
		if v > 1 {
			count++
		}
	}

	return count
}

func part1and2() {
	filename := "./data/input.txt"

	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("error opening file %v. err = %v\n", filename, err)
	}
	defer file.Close()

	overlaps := newOverlaps()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()
		pointStrs := strings.Split(line, " -> ")

		if len(pointStrs) != 2 {
			fmt.Printf("incorrect number of points. line = %v\n", line)
			return
		}

		startPointStrings := strings.Split(pointStrs[0], ",")
		endPointStrings := strings.Split(pointStrs[1], ",")

		if len(startPointStrings) != 2 || len(endPointStrings) != 2 {
			fmt.Printf("Wrong number of x and y values in start=%v or end=%v\n", startPointStrings, endPointStrings)
			return
		}

		startPoint := newPoint(AtoiPanic(startPointStrings))
		endPoint := newPoint(AtoiPanic(endPointStrings))

		ventLine := newVentLine(*startPoint, *endPoint)
		overlaps.plotVentLine(*ventLine)
	}

	// all points plotted
	fmt.Printf("there are %d overlaps\n", overlaps.countCrossovers())
}

func AtoiPanic(str []string) (int, int) {
	x, err := strconv.Atoi(str[0])
	if err != nil {
		panic(err)
	}

	y, err := strconv.Atoi(str[1])
	if err != nil {
		panic(err)
	}
	return x, y
}

func main() {
	part1and2()
}
