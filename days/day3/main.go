package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type columnCounter struct {
	counts    []int // the total for each column
	totalRows int   // the number of rows read for the counts
}

func newColumnCounter() *columnCounter {
	return &columnCounter{
		counts:    make([]int, 0),
		totalRows: 0,
	}
}

// countColumns adds the value in each column to the overall count
func (c *columnCounter) countColumns(binString string) error {
	var num int
	var err error
	for pos, ch := range binString {
		num, err = strconv.Atoi(fmt.Sprintf("%c", ch))
		if err != nil || (num != 0 && num != 1) {
			return fmt.Errorf("the string %v is not a binary representation", binString)
		}
		if pos < len(c.counts) {
			c.counts[pos] = c.counts[pos] + num
		} else {
			c.counts = append(c.counts, num)
		}
	}
	c.totalRows++
	return nil
}

func (c *columnCounter) countColumnsArray(binString []string) error {
	for _, str := range binString {
		if err := c.countColumns(str); err != nil {
			return err
		}
	}

	return nil
}

// mostCommonBit returns true if '1' is most common for the column or false for '0'
func (c *columnCounter) mostCommonBit(col int) (bool, error) {
	if col < 0 || col > (len(c.counts)-1) {
		return false, fmt.Errorf("there is no column %d", col)
	}

	if (c.totalRows - c.counts[col]) <= c.counts[col] {
		// more 1's than zeros
		return true, nil
	} else {
		// more 0's than ones
		return false, nil
	}
}

func (c *columnCounter) getGammaAndEpsilonRates() (int64, int64, error) {
	// build a string based on the true/false values
	var gammaBuilder strings.Builder
	var epsilonBuilder strings.Builder
	var colbit bool
	var err error
	for i := 0; i < len(c.counts); i++ {
		if colbit, err = c.mostCommonBit(i); err != nil {
			return 0, 0, err
		} else if colbit {
			gammaBuilder.WriteString("1")
			epsilonBuilder.WriteString("0")
		} else {
			gammaBuilder.WriteString("0")
			epsilonBuilder.WriteString("1")
		}
	}

	gamma, err := strconv.ParseInt(gammaBuilder.String(), 2, 64)
	if err != nil {
		return 0, 0, fmt.Errorf("error parsing gamma string %s. err = %v", gammaBuilder.String(), err)
	}

	epsilon, err := strconv.ParseInt(epsilonBuilder.String(), 2, 64)
	if err != nil {
		return 0, 0, fmt.Errorf("error parsing epsilon string %s. err = %v", epsilonBuilder.String(), err)
	}

	return gamma, epsilon, nil
}

// readBinaryStrings reads strings representing binary numbers (e.g. "10101")
func readBinaryStrings(filename string) (*columnCounter, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	columnCounter := newColumnCounter()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		str := scanner.Text()
		err = columnCounter.countColumns(str)
		if err != nil {
			return nil, err
		}
	}

	return columnCounter, nil
}

func main() {
	part1()
	fmt.Println("- - - - - - -")
	part2()
}

func part1() {
	filename := "./data/input.txt"
	columnCounter, err := readBinaryStrings(filename)
	if err != nil {
		log.Fatalf("error reading file of binary ints: %v", err)
	}

	gamma, epsilon, err := columnCounter.getGammaAndEpsilonRates()
	if err != nil {
		log.Fatalf("error getting rates. err = %v", err)
	}

	fmt.Printf("gamma = %v, epsilon = %v and power consumption = %v\n", gamma, epsilon, gamma*epsilon)
}
