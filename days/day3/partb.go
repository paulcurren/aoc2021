package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func loadStringArray(filename string) ([]string, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var contents = make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		contents = append(contents, scanner.Text())
	}

	return contents, nil
}

func filterToPrefix(list []string, prefix string) []string {
	filtered := make([]string, 0)

	for _, str := range list {
		if strings.HasPrefix(str, prefix) {
			filtered = append(filtered, str)
		}
	}

	return filtered
}

// calculateLifeSupport calculates oxygen is that parameter is true, or CO2 scrubber if oxygen is false
func calculateLifeSupport(filename string, oxygen bool) (int, error) {
	var bins []string
	var err error

	if bins, err = loadStringArray(filename); err != nil {
		return 0, err
	}

	// init - count initial set of columns and set up columnPos limit
	counter := newColumnCounter()
	if err = counter.countColumnsArray(bins); err != nil {
		return 0, err
	}

	columnLimit := len(counter.counts)
	prefixStr := ""

	// for each column find the most common bit then filter, and repeat
	for columnPos := 0; columnPos < columnLimit; columnPos++ {
		var bit bool
		if bit, err = counter.mostCommonBit(columnPos); err != nil {
			return 0, err
		}

		if bit {
			if oxygen {
				prefixStr += "1"
			} else {
				prefixStr += "0"
			}
		} else {
			if oxygen {
				prefixStr += "0"
			} else {
				prefixStr += "1"
			}
		}

		bins = filterToPrefix(bins, prefixStr)
		if len(bins) == 1 {
			break
		}

		counter = newColumnCounter()
		counter.countColumnsArray(bins)
	}

	if len(bins) != 1 {
		return 0, fmt.Errorf("multiple binary values left after counting all columns. %d values", len(bins))
	} else {
		var numeric int64
		if numeric, err = strconv.ParseInt(bins[0], 2, 32); err != nil {
			return 0, err
		}
		return int(numeric), nil
	}
}

func part2() {
	filename := "./data/input.txt"
	oxygen, err := calculateLifeSupport(filename, true)
	if err != nil {
		fmt.Printf("error calculating oxygen. err = %v\n", err)
	} else {
		fmt.Printf("oxygen = %d\n", oxygen)
	}

	co, err := calculateLifeSupport(filename, false)
	if err != nil {
		fmt.Printf("error calculating co2 scrubber. err = %v\n", err)
	} else {
		fmt.Printf("co2 scrubber = %d\n", co)
	}

	fmt.Printf("life support rating = %d\n", oxygen*co)
}
