package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type position struct {
	horizontal int
	aim        int
	depth      int
}

func (p *position) answer() int {
	return p.horizontal * p.depth
}

func (p *position) executeSingle(c command) error {
	switch c.instruction {
	case "forward":
		p.horizontal += c.amount
	case "down":
		p.depth += c.amount
	case "up":
		p.depth -= c.amount
	default:
		return fmt.Errorf("unknown command %v", c.instruction)
	}

	return nil
}

func (p *position) executeSingleWithAim(c command) error {
	switch c.instruction {
	case "forward":
		p.horizontal += c.amount
		p.depth = p.depth + (p.aim * c.amount)
	case "down":
		p.aim += c.amount
	case "up":
		p.aim -= c.amount
	default:
		return fmt.Errorf("unknown command %v", c.instruction)
	}

	return nil
}

func (p *position) executeSeries(commands []command) error {
	for _, c := range commands {
		err := p.executeSingle(c)
		if err != nil {
			return fmt.Errorf("error executing command %v", c)
		}
	}

	return nil
}

func (p *position) executeSeriesWithAim(commands []command) error {
	for _, c := range commands {
		err := p.executeSingleWithAim(c)
		if err != nil {
			return fmt.Errorf("error executing command %v", c)
		}
	}

	return nil
}

type command struct {
	instruction string
	amount      int
}

// NewFromLine parses a line of format '<instruction> <amount>' and creates a command from it
func NewFromLine(line string) (*command, error) {
	parts := strings.Split(line, " ")

	if len(parts) != 2 {
		return nil, fmt.Errorf("the line did not contain 2 parts. contained %v", line)
	}

	numAmount, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}

	return &command{
		instruction: parts[0],
		amount:      numAmount,
	}, nil
}

func readCommands(filename string) ([]command, error) {
	file, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	commands := make([]command, 0, 50)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		command, err := NewFromLine(scanner.Text())
		if err != nil {
			return nil, fmt.Errorf("error reading file %v. err = %v", filename, err)
		}
		commands = append(commands, *command)
	}

	return commands, nil
}

func main() {
	part1()
	part2()
}

func part1() {
	filename := "./data/input.txt"
	commands, err := readCommands(filename)
	if err != nil {
		log.Fatalf("error reading input file %v. err = %v", filename, err)
	}

	pos := &position{}
	err = pos.executeSeries(commands)

	if err != nil {
		log.Fatalf("error executing series of commands: %v", err)
	}

	fmt.Printf("part 1 answer = %d\n", pos.answer())
}

func part2() {
	filename := "./data/input.txt"
	commands, err := readCommands(filename)
	if err != nil {
		log.Fatalf("error reading input file %v. err = %v", filename, err)
	}

	pos := &position{}
	err = pos.executeSeriesWithAim(commands)

	if err != nil {
		log.Fatalf("error executing series of commands: %v", err)
	}

	fmt.Printf("part 2 answer = %d\n", pos.answer())
}
