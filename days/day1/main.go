package main

import (
	"bufio"
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
)

const NO_LAST_NUM = -1 // -1 indicates no last number
// the first entry is an example. We also set this on nan

type fileCounts struct {
	increase int
	decrease int
	same     int
	nan      int
	entries  int
}

type triple struct {
	one   int
	two   int
	three int
}

func NewTriple() *triple {
	return &triple{NO_LAST_NUM, NO_LAST_NUM, NO_LAST_NUM}
}

func (t *triple) addNum(n int) {
	if t.isPopulated() {
		// shuffle all the numbers, dropping the first
		t.one = t.two
		t.two = t.three
		t.three = n
	} else if t.one == NO_LAST_NUM {
		t.one = n
	} else if t.two == NO_LAST_NUM {
		t.two = n
	} else if t.three == NO_LAST_NUM {
		t.three = n
	}
}

func (t *triple) isPopulated() bool {
	return t.one != NO_LAST_NUM && t.two != NO_LAST_NUM && t.three != NO_LAST_NUM
}

func (t *triple) total() int {
	// should check for NO_LAST_NUM
	return t.one + t.two + t.three
}

func (t *triple) clone() *triple {
	return &triple{
		one:   t.one,
		two:   t.two,
		three: t.three,
	}
}

func main() {
	counts, err := part1("./data/input.txt")
	if err != nil {
		fmt.Printf("part1 error = %v\n", err)
	} else {
		fmt.Printf("part 1 counts = %v\n", counts)
	}

	counts, err = part2("./data/input.txt")
	if err != nil {
		fmt.Printf("part2 error = %v\n", err)
	} else {
		fmt.Printf("part 2 counts = %v\n", counts)
	}
}

func part1(filename string) (*fileCounts, error) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	counts := &fileCounts{}
	scanner := bufio.NewScanner(file)
	lastnum := NO_LAST_NUM
	for scanner.Scan() {
		line := scanner.Text()
		counts.entries++
		num, err := strconv.Atoi(line)
		if err != nil {
			counts.nan++
			lastnum = NO_LAST_NUM
			continue
		}

		if num < 0 {
			return counts, errors.New("encountered number <0. change the logic paul")
		}

		if lastnum != NO_LAST_NUM {
			if num > lastnum {
				counts.increase++
			} else if num < lastnum {
				counts.decrease++
			} else if num == lastnum {
				counts.same++
			}
		}

		lastnum = num
	}
	if err := scanner.Err(); err != nil {
		return counts, fmt.Errorf("error scanning file %v", err)
	}

	return counts, nil
}

func part2(filename string) (*fileCounts, error) {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	counts := &fileCounts{}
	scanner := bufio.NewScanner(file)
	var lastTriple *triple
	currentTriple := NewTriple()
	for scanner.Scan() {
		line := scanner.Text()
		counts.entries++
		num, err := strconv.Atoi(line)
		if err != nil {
			counts.nan++
			continue
		}

		if currentTriple.isPopulated() {
			lastTriple = currentTriple.clone()
			currentTriple.addNum(num)
			current := currentTriple.total()
			lastnum := lastTriple.total()
			if current > lastnum {
				counts.increase++
			} else if current < lastnum {
				counts.decrease++
			} else if current == lastnum {
				counts.same++
			}
		} else {
			currentTriple.addNum(num)
		}
	}
	if err := scanner.Err(); err != nil {
		return counts, fmt.Errorf("error scanning file %v", err)
	}

	return counts, nil
}
