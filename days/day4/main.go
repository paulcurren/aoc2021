package main

import (
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

type bingoCard struct {
	board      [][]int
	marks      [][]bool
	winningNum int
}

func newBingoCardFromLines(lines []string) (*bingoCard, error) {
	if len(lines) != 5 {
		return nil, fmt.Errorf("the wrong number of lines were supplied. num of lines = %d", len(lines))
	}

	card := make([][]int, 0, 5)

	for _, lineStr := range lines {
		line := make([]int, 0, 5)
		scanner := bufio.NewScanner(strings.NewReader(lineStr))
		scanner.Split(bufio.ScanWords)
		for scanner.Scan() {
			word := scanner.Text()
			num, err := strconv.Atoi(word)
			if err != nil {
				return nil, fmt.Errorf("could not convert word %v to an int", word)
			}
			line = append(line, num)
		}

		if len(line) != 5 {
			return nil, fmt.Errorf("the line had the wrong number of numbers. was = %d", len(line))
		}
		card = append(card, line)
	}

	marks := make([][]bool, 5)
	for i := 0; i < 5; i++ {
		marksLine := make([]bool, 5)
		marks[i] = marksLine
	}

	return &bingoCard{
		board: card,
		marks: marks,
	}, nil
}

// mark mark the number on the card and return true if the board has a complete horizontal or vertical line
func (b *bingoCard) mark(num int) bool {
	// iterate over all the numbers looking for a match
	match := false
	for vpos := 0; vpos < len(b.board) && !match; vpos++ {
		for hpos := 0; hpos < len(b.board[vpos]); hpos++ {
			if b.board[vpos][hpos] == num {
				b.marks[vpos][hpos] = true
				match = true
				break
			}
		}
	}

	if match {
		winner := b.check()
		if winner {
			b.winningNum = num
		}
		return winner
	}

	return false
}

// check returns true if the card is a winner
func (b *bingoCard) check() bool {
	// check each line first
	for _, line := range b.marks {
		check := true
		for _, m := range line {
			check = check && m
		}

		if check {
			return true
		}
	}

	// no winning line; check columns
	for i := 0; i < len(b.marks[0]); i++ {
		check := true
		for _, line := range b.marks {
			check = check && line[i]
		}

		if check {
			return true
		}
	}

	return false
}

// score will add all the unmarked numbers on a board and multiply by the winning number
func (b *bingoCard) score() int {
	sum := 0
	for v := 0; v < len(b.marks); v++ {
		for h := 0; h < len(b.marks[v]); h++ {
			if !b.marks[v][h] {
				sum += b.board[v][h]
			}
		}
	}

	return sum * b.winningNum
}

func part1() {
	filename := "./data/input.txt"
	cards, drawnNums, err := loader(filename)
	if err != nil {
		fmt.Printf("error reading data file %v. err = %v\n", filename, err)
		return
	}

	// iterate over the drawn numbers marking each board and stopping when there is a winner.
	for _, n := range drawnNums {
		for _, board := range cards {
			if board.mark(n) {
				fmt.Printf("Winning board. score = %v\n", board.score())
				return
			}
		}
	}

	fmt.Println("no winning boards found")

}

func part2() {
	filename := "./data/input.txt"
	cards, drawnNums, err := loader(filename)
	if err != nil {
		fmt.Printf("error reading data file %v. err = %v\n", filename, err)
		return
	}

	// last board to win
	// keep track of what boards have won
	// stop drawing numbers once all boards have won and score that final board
	finishedBoards := make([]bool, len(cards)) // all false to start
	for _, n := range drawnNums {
		for i := 0; i < len(cards); i++ {
			if cards[i].mark(n) {
				fmt.Printf("board number %d has won.\n", i)
				finishedBoards[i] = true

				allboardsFinished := true
				for _, f := range finishedBoards {
					allboardsFinished = allboardsFinished && f
				}

				if allboardsFinished {
					// score board i and return
					fmt.Printf("The final board to finish was %d. score = %d\n", i, cards[i].score())
					return
				}
			}
		}
	}
}

func main() {
	part1()
	part2()
}
