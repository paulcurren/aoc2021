package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func loader(filename string) ([]*bingoCard, []int, error) {
	// scan lines.
	// first line is the drawn numbers
	file, err := os.Open(filename)
	if err != nil {
		return nil, nil, err
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	if !scanner.Scan() {
		return nil, nil, fmt.Errorf("no lines in the input file %v", filename)
	}

	drawnLine := scanner.Text()
	strArr := strings.Split(drawnLine, ",")
	drawnNumbers := make([]int, len(strArr))
	for i := 0; i < len(strArr); i++ {
		if drawnNumbers[i], err = strconv.Atoi(strArr[i]); err != nil {
			return nil, nil, fmt.Errorf("non numeric drawn number entry of %v", strArr[i])
		}
	}

	// now read the boards, blank lines between each
	cards := make([]*bingoCard, 0)
	var currentLines []string

	for scanner.Scan() {
		line := scanner.Text()
		if line == "" {
			currentLines = make([]string, 0, 5)
		} else {
			currentLines = append(currentLines, line)
		}

		if len(currentLines) == 5 {
			// that's a complete card
			newCard, err := newBingoCardFromLines(currentLines)
			if err != nil {
				return nil, nil, fmt.Errorf("error creating a card from the lines. err = %v", err)
			}

			cards = append(cards, newCard)
		}
	}

	return cards, drawnNumbers, nil
}
